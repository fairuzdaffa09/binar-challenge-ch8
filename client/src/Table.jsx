const Table = ({
    players = [],
    onChange
}) => {
    return(
        <div>
            <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Password</th>
                        <th>Exp</th>
                        <th>Level</th>
                    </tr>
                </thead>
                <tbody>
                    {players.map((data, index) => (
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{data.username}</td>
                            <td>{data.email}</td>
                            <td>{data.password}</td>
                        </tr>
                    ))}
                    </tbody>

            </table>
        </div>
    )
}

export default Table;
